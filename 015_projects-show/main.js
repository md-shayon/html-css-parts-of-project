const title_desc = document.querySelectorAll('.title-desc');
project_image = document.querySelectorAll('.project-image');
// console.log(project_image);


title_desc.forEach((td, i) => {
    // td.style.display = "none";
    td.classList.add('hide');
});

reset();
function reset(){
    title_desc.forEach((td, i) => {
        // td.style.display = "none";
        td.classList.add('hide');
        td.classList.remove('show');
    });    
}


project_image.forEach((pi, i)=>{
    pi.addEventListener('mouseover', (e)=>{
        // console.log(e.target);
        // console.log(e.target.classList.contains("project-image"));
        if(e.target.classList.contains("project-image")){
            e.target.querySelector('.title-desc').classList.remove('hide');
            // e.target.querySelector('.title-desc').className = "title-desc show";
            e.target.querySelector('.title-desc').classList.add('show');
        }

    });
    pi.addEventListener('mouseout', (e)=>{

        reset();
    });
    pi.addEventListener('mouseleave', (e)=>{
        reset();
    });
});