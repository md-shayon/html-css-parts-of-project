// Prevent main image loading top to bottom
// Instead it should load at once

const imgWrapEl = document.querySelectorAll('.img-wrap');
imgWrapEl.forEach((iwe)=>{
    const imgEl = iwe.querySelector('img');

    function loaded(){
        // Show image
        iwe.classList.add('loaded');
    }

    // Check image is downloaded or not
    if (imgEl.complete){
        loaded();
    }else {
        // As soon as image is done loading it is going call the function
        imgEl.addEventListener('load', loaded);
    }
});