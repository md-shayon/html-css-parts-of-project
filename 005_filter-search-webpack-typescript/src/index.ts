import districtList from "./district-list";

const searchFilterForm = document.getElementById("search-filter-form");
const districtInput = document.getElementById("district-input");
const searchResult = document.getElementById("search-result");


interface DistrictDataInt {
  name: string;
  charge: number;
}

function buildList(districtListData: DistrictDataInt[]) {
  searchResult.innerHTML = null;
  searchFilterForm.addEventListener("submit", (event) => {
    event.preventDefault();
    console.log(districtListData, districtInput);
  });

  for (let i = 0; i < districtListData.length; i += 1) {
    const liEl = document.createElement("li");
    liEl.setAttribute("class", "list-group-item");
    liEl.textContent = `${districtListData[i].name}  ৳${districtListData[i].charge}`;
    searchResult.appendChild(liEl);
  }
}

buildList(districtList);

districtInput.addEventListener("keyup", (event: KeyboardEvent) => {
  event.preventDefault();
  const val = (event.target as HTMLInputElement).value;
  
  
  let newDistrictList = structuredClone(districtList);

  const filteredDistrictList = newDistrictList.filter((district) =>
    district.name.toLowerCase().includes(val.toLowerCase())
  );
  
  buildList(filteredDistrictList);
  newDistrictList = structuredClone(districtList);
});
